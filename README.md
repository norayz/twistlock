# Twistlock

That is my first go program. Actually, I've learnt go specially for that task and Twistlock :)
It was written on Arch Linux, assuming words_clean.txt is at the same folder with main.go.
Also, I am assuming that the database contains only lower-case words. I have not tested against upper-case database.

## Stress Tests

For stress tests I added a new route: /similarRandom, which generates a string from the database and then gets similars of him.
I am using *Artillery* for the tests. Install it by: npm install -g artillery, and use it by: artillery run artillery.yml at the main folder.
The case I am testing is 60s with 1000 new users arriving each sec.
After that check /stats.

## Design Strategy

After reading the database file, I am loading all of its content to a hash table that saved in the RAM.
The hash function I am using is a self-made function in purpose to return the same code for each similarity (permutation) family of words.
For example, hash("abc")=hash("cba")=hash("bac") and etc..
In gist, it is simply fnv-hash on the sorted word. Note that the sorting takes O(length(word)).

I was searching online for a hash table with a linked-list (or a tree) for competing with collisions. 
Unfortunatly, I couldn't find. So, I've decided to use go's built-in map, which is bit shitty because it already hashes by its own.
That means, it hashes my already hashed value, and that is a waste of CPU and could make unwanted collisions.

## Possible Improvements
1. Implement my own hash table?
2. Maybe a faster hash function (without sort)?
3. Save database's hash table to the disk (instead of creating it at each run)
4. Add some tests
5. I think concurrency wouldn't make that application much better. That why I chose to simplify the implementation and abandon go-routines.
6. I have to to decide about dealing with collisons and false-positives. Is that really necessary?

Looking forward to discuss with you.
Yaron.