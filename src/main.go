package main

import (
	"log"
)

const (
	port                 = 8000
	databaseFilePath     = "../words_clean.txt"
	databaseFileDelimter = "\n"
)

func main() {
	database, err := readDatabase(databaseFilePath, databaseFileDelimter)
	if err != nil {
		log.Fatal(err)
	}
	similarsMap := createSimilarsMap(database)

	initAPI(port, database, similarsMap)
}
