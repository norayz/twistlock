package main

func createSimilarsMap(database []string) map[string][]string {
	similarsMap := make(map[string][]string)

	for _, word := range database {
		sorted := countSort(word)

		similars, isExist := similarsMap[sorted]
		if isExist {
			similars = append(similars, word)
		} else {
			similars = []string{word}
		}
		similarsMap[sorted] = similars
	}

	return similarsMap
}

func getSimliars(str string, similarsMap map[string][]string) []string {
	sorted := countSort(str)
	similars, isExist := similarsMap[sorted]
	if !isExist {
		return []string{}
	}

	similars = removeString(similars, str)
	return similars
}

func removeString(arr []string, str string) []string {
	for i, curr := range arr {
		if curr == str {
			arr[i] = arr[len(arr)-1]
			return arr[:len(arr)-1]
		}
	}
	return arr
}
