package main

import (
	"strings"
)

func countSort(str string) string {
	var counts [26]int
	for _, ch := range str {
		counts[ch-'a']++
	}

	var builder strings.Builder
	for ch := 'a'; ch <= 'z'; ch++ {
		builder.WriteString(strings.Repeat(string(ch), counts[ch-'a']))
	}

	return builder.String()
}
