package main

import (
	"testing"
)

// TestDatabaseSize he
func TestDatabaseWordsCleanSize(t *testing.T) {
	const expected = 351075

	database, err := readDatabase("../words_clean.txt", "\n")
	if err != nil {
		t.Error(err)
	}

	actual := len(database)
	if actual != expected {
		t.Errorf("Database size was incorrect, actual: %d, expected: %d", actual, expected)
	}
}

func TestDatabaseFileNotExists(t *testing.T) {
	_, err := readDatabase("fake.txt", databaseFileDelimter)
	if err == nil {
		t.Error(err)
	}
}
