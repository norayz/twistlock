package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/nu7hatch/gouuid"
)

type (
	similarResponse struct {
		Similar []string `json:"similar"`
	}

	statsResponse struct {
		TotalWords          int `json:"totalWords"`
		TotalRequests       int `json:"totalRequests"`
		AvgProcessingTimeNs int `json:"avgProcessingTimeNs"`
	}
)

var totalRequests int
var totalTimeNs time.Duration

func initAPI(port int, database []string, similarsMap map[string][]string) {
	r := mux.NewRouter()
	v1 := r.PathPrefix("/api/v1").Subrouter()
	v1.HandleFunc("/similar", handleSimiliarClosure(similarsMap))
	v1.HandleFunc("/similarRandom", handleSimiliarRandomClosure(database, similarsMap))
	v1.HandleFunc("/stats", handleStatsClosure(database, similarsMap))

	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", port), r))
}

func handleSimiliarClosure(similarsMap map[string][]string) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		query := r.URL.Query()
		word := query.Get("word")
		handleSimilar(word, similarsMap, w, r)
	}
}

func handleSimiliarRandomClosure(database []string, similarsMap map[string][]string) func(http.ResponseWriter, *http.Request) {
	rand.Seed(time.Now().Unix())

	return func(w http.ResponseWriter, r *http.Request) {
		word := database[rand.Intn(len(database))]
		handleSimilar(word, similarsMap, w, r)
	}
}

func handleSimilar(word string, similarsMap map[string][]string, w http.ResponseWriter, r *http.Request) {
	start := time.Now()

	uuid, err := uuid.NewV4()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	log.Printf("Similar request (%s) has started (word: %s)\n", uuid, word)

	similars := getSimliars(word, similarsMap)
	response := &similarResponse{similars}
	jsonResponse, err := json.Marshal(response)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write(jsonResponse)

	elapsed := time.Since(start)
	log.Printf("Similar request (%s) has finished (elapsed: %s)\n", uuid, elapsed)

	totalTimeNs += elapsed
	totalRequests++
}

func handleStatsClosure(database []string, similarsMap map[string][]string) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		response := createStatsResponse(database, totalTimeNs, totalRequests)
		jsonResponse, err := json.Marshal(response)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Write(jsonResponse)
	}
}

func createStatsResponse(database []string, totalTimeNs time.Duration, totalRequests int) *statsResponse {
	totalWords := len(database)

	var avgProcessingTimeNs int
	if totalRequests == 0 {
		avgProcessingTimeNs = 0
	} else {
		avgProcessingTimeNs = int(totalTimeNs) / totalRequests
	}

	response := &statsResponse{totalWords, totalRequests, avgProcessingTimeNs}
	return response
}
