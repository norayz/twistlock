package main

import (
	"io/ioutil"
	"strings"
)

func readDatabase(filePath string, delimter string) (database []string, err error) {
	content, err := ioutil.ReadFile(filePath)
	if err != nil {
		return
	}

	database = strings.Split(string(content), delimter)
	return
}
